﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaxexColorPicker.Model
{
    /// <summary>
    /// Model for create color palette
    /// </summary>
    public class PredefinedColors
    {
        #region Properties

        private List<CustomColor> colors;

        public List<CustomColor> Colors
        {
            get { return colors; }
            set { colors = value; }
        }

        #endregion

        #region Constructor

        public PredefinedColors()
        {
            colors = new List<CustomColor>();

            FillPalette();
        }

        #endregion

        #region Methods

        private void FillPalette()
        {
            //Column 1
            colors.Add(new CustomColor("Black", "#000000"));
            colors.Add(new CustomColor("White", "#FFFFFF"));
            colors.Add(new CustomColor("Red", "#FF0000"));
            colors.Add(new CustomColor("Green", "#00FF00"));

            //Column 2
            colors.Add(new CustomColor("Blue", "#0000FF"));
            colors.Add(new CustomColor("Yellow", "#FFFF00"));
            colors.Add(new CustomColor("Magenta", "#FF00FF"));
            colors.Add(new CustomColor("Cyan", "#00FFFF"));


            //Additional Colors
#if DEBUG

            colors.Add(new CustomColor("Dark Red", "#800000"));
            colors.Add(new CustomColor("Dark Blue", "#003366"));
            colors.Add(new CustomColor("Gray", "#C0C0C0"));
            colors.Add(new CustomColor("Dark Gray", "#808080"));

#endif
        }


        #endregion
    }
}
