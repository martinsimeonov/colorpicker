﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaxexColorPicker.Model
{
    /// <summary>
    /// Model for defining color in palette
    /// </summary>
    public class CustomColor
    {
        #region Properties

        private string colorTitle;
        private string colorContent;

        /// <summary>
        /// Title for color
        /// </summary>
        public string ColorTitle
        {
            get { return colorTitle; }
            set { colorTitle = value; }
        }

        /// <summary>
        /// HTML value for color
        /// </summary>
        public string ColorContent
        {
            get { return colorContent; }
            set { colorContent = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Parameterless constructor
        /// </summary>
        public CustomColor()
        {
            colorTitle = "Black";
            colorContent = "#000000";
        }

        /// <summary>
        /// Constructor for setting up color
        /// </summary>
        /// <param name="Title">Title of color</param>
        /// <param name="Content">HTML color in string</param>
        public CustomColor (string Title, string Content)
        {
            colorTitle = Title;
            colorContent = Content;
        }

        #endregion
    }
}
