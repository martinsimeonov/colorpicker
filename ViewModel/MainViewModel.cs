﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

//Helpers
using NaxexColorPicker.Helpers;

namespace NaxexColorPicker.ViewModel
{
    /// <summary>
    /// Main ViewModel
    /// </summary>
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Model

        /// <summary>
        /// Model that contains color palette
        /// <remarks>
        /// In Debug mode is adding additional colors for testing UI
        /// </remarks>
        /// </summary>
        private Model.PredefinedColors PredefinedColors;

        #endregion

        #region Properties


        private ObservableCollection<Model.CustomColor> colors;

        public ObservableCollection<Model.CustomColor> Colors
        {
            get
            {
                if (colors == null)
                    colors = new ObservableCollection<Model.CustomColor>();
                else
                    colors.Clear();

                foreach (var item in PredefinedColors.Colors)
                    colors.Add(item);

                return colors;
            }
        }

        private byte red;

        public byte Red
        {
            get {
                return red;
                //return selectedColor.getRed ();
            }
            set {
                red = value;

                SelectedColor = HTMLColorHelper.GetHtmlColorFromRgb(red, green, blue);
            }
        }

        private byte green;

        public byte Green
        {
            get { return green;
            }
            set {
                green = value;

                SelectedColor = HTMLColorHelper.GetHtmlColorFromRgb(red, green, blue);
            }
        }


        private byte blue;

        public byte Blue
        {
            get {
                return blue;
            }
            set {
                blue = value;

                SelectedColor = HTMLColorHelper.GetHtmlColorFromRgb(red, green, blue);;
            }
        }
        

        private string selectedColor;

        public string SelectedColor
        {
            get { return selectedColor; }
            set {
                selectedColor = value;

                OnPropertyChanged("SelectedColor");
                OnPropertyChanged("Red");
                OnPropertyChanged("Green");
                OnPropertyChanged("Blue");
            }
        }

        #endregion

        #region Constructor

        public MainViewModel()
        {
            //Model
            PredefinedColors = new Model.PredefinedColors();

            //Selecting color
            if (PredefinedColors.Colors.Count > 0)
                selectedColor = PredefinedColors.Colors[0].ColorContent;
            else
                selectedColor = "#000000";

            //Commands
            SelectColorCommand = new Commands.SelectColorCommand(this);
            CopyColorCommand = new Commands.CopyColorCommand(this);
            CloseCommand = new Commands.CloseCommand(this);
            
        }

        #endregion

        #region Commands

        public ViewModel.Commands.SelectColorCommand SelectColorCommand { get; set; }
        public ViewModel.Commands.CopyColorCommand CopyColorCommand { get; set; }
        public ViewModel.Commands.CloseCommand CloseCommand { get; set; }

        public void selectColor (string htmlColor)
        {
            Console.WriteLine("HTML Color: {0}", htmlColor);

            red = htmlColor.getRed();
            green = htmlColor.getGreen();
            blue = htmlColor.getBlue();

            SelectedColor = htmlColor;
            
        }

        public void closeApplication ()
        {
            Application.Current.MainWindow.Close();
        }

        public void copyToClipboard ()
        {
            Clipboard.SetDataObject(SelectedColor);
        }

        #endregion

        #region INotifyPropertyChanged Имплементация

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
