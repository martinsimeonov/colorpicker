﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

//Helper
using NaxexColorPicker.Helpers;


namespace NaxexColorPicker.ViewModel.Converters
{
    /// <summary>
    /// Onew way converter for HTML color in string format and Brush
    /// </summary>
    public class HtmlColorToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BrushConverter brushConverter = new BrushConverter();
            string htmlColor = "#000000";

            if (value != null)
            {

                if (value is string)
                {
                    if (((string)value).IsValidHtmlColor())
                    {
                        htmlColor = (string)value;
                    }

                }
            }
            
            
            return (Brush)brushConverter.ConvertFrom(htmlColor);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
