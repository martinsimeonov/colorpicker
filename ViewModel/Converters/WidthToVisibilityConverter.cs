﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace NaxexColorPicker.ViewModel.Converters
{
    /// <summary>
    /// This one way conveter check if value is less then parameter. Both is must
    /// be in double format.
    /// 
    /// If condition is true then retrun Collapsed. In other cases return Visibile.
    /// 
    /// This converter is used for creating a basic relative behaivor.
    /// </summary>
    public class WidthToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || parameter == null) return Visibility.Visible;

            if (value is double && parameter is double)
            {

                if ( (double) value <= (double) parameter)
                    return Visibility.Collapsed;
            }
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
