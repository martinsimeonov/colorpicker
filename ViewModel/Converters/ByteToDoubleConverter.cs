﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace NaxexColorPicker.ViewModel.Converters
{
    /// <summary>
    /// Two way converter for byte and double
    /// </summary>
    public class ByteToDoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                if (value is byte)
                    return System.Convert.ToDouble((byte)value);
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            byte convertedValue = 0;

            if (value != null)
            {
                if (value is double)
                    convertedValue = System.Convert.ToByte ( (double) value);
            }

            return convertedValue;
        }
    }
}
