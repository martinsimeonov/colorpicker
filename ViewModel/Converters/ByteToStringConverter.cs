﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace NaxexColorPicker.ViewModel.Converters
{
    /// <summary>
    /// Two way converter for byte and string
    /// </summary>
    public class ByteToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                if (value is byte)
                    return ((byte)value).ToString();
            }

            return "0";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Byte convertedValue = 0;

            if (value != null)
            {
                if (value is string)
                    Byte.TryParse((string)value, out convertedValue);
            }

            return convertedValue;
        }
    }
}
