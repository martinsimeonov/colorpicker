﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using NaxexColorPicker.Helpers;

namespace NaxexColorPicker.ViewModel.Commands
{
    /// <summary>
    /// Command for checking and copyng to clipboard html color
    /// </summary>
    public class CopyColorCommand : ICommand
    {
        public MainViewModel ViewModel { get; set; }

        public CopyColorCommand(MainViewModel viewModel)
        {
            ViewModel = viewModel;

        }
        public bool CanExecute(object parameter)
        {

            if (ViewModel.SelectedColor == null) return false;
            if (! ViewModel.SelectedColor.IsValidHtmlColor()) return false;

            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            ViewModel.copyToClipboard();
        }
    }
}
