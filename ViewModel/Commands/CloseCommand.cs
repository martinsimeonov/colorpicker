﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using NaxexColorPicker.Helpers;

namespace NaxexColorPicker.ViewModel.Commands
{
    /// <summary>
    /// Command for closing Apllication
    /// </summary>
    public class CloseCommand : ICommand
    {
        public MainViewModel ViewModel { get; set; }

        public CloseCommand(MainViewModel viewModel)
        {
            ViewModel = viewModel;

        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            ViewModel.closeApplication();
        }
    }
}
