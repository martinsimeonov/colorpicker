﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using NaxexColorPicker.Helpers;

namespace NaxexColorPicker.ViewModel.Commands
{
    /// <summary>
    /// Command for selecting color
    /// </summary>
    public class SelectColorCommand : ICommand
    {
        public MainViewModel ViewModel { get; set; }

        public SelectColorCommand(MainViewModel viewModel)
        {
            ViewModel = viewModel;

        }
        public bool CanExecute(object parameter)
        {
            if (parameter == null) return false;
            if (!(parameter is string)) return false;
            if (!((string)parameter).IsValidHtmlColor()) return false;

            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            ViewModel.selectColor((string)parameter);
        }
    }
}
