﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Drawing;

namespace NaxexColorPicker.Helpers
{
    /// <summary>
    /// Helper for working and validating html colors in string format.
    /// </summary>
    public static class HTMLColorHelper
    {
        /// <summary>
        /// Method for validating HTML color.
        /// </summary>
        /// <param name="inputColor">HTML Color</param>
        /// <returns>Returns true if inputColor is valid</returns>
        public static bool IsValidHtmlColor(this string inputColor)
        {
            if (Regex.Match(inputColor, "^#(?:[0-9a-fA-F]{3}){1,2}$").Success)
                return true;

            return false;
        }

        /// <summary>
        /// Method for getting HTML Color from Red, Green and Blue parameter
        /// in byte format
        /// </summary>
        /// <param name="R">Red</param>
        /// <param name="G">Green</param>
        /// <param name="B">Blue</param>
        /// <returns>HTML Color in string</returns>
        public static string GetHtmlColorFromRgb (byte R, byte G, byte B)
        {
            Color color = new Color();

            color = Color.FromArgb(0, R, G, B);

            return ColorTranslator.ToHtml(color);
        }

        /// <summary>
        /// Mothod for getting Red parameter in byte format
        /// from HTML Color in string format
        /// </summary>
        /// <param name="inputColor">HTML Color</param>
        /// <returns>Red parameter</returns>
        public static byte getRed (this string inputColor)
        {
            if (inputColor == null) return 0;
            if (!inputColor.IsValidHtmlColor()) return 0;

            Color color = ColorTranslator.FromHtml(inputColor);

            return color.R;
        }

        /// <summary>
        /// Mothod for getting Green parameter in byte format
        /// from HTML Color in string format
        /// </summary>
        /// <param name="inputColor">HTML Color</param>
        /// <returns>Green parameter</returns>
        public static byte getGreen(this string inputColor)
        {
            if (inputColor == null) return 0;
            if (!inputColor.IsValidHtmlColor()) return 0;

            Color color = ColorTranslator.FromHtml(inputColor);

            return color.G;
        }

        /// <summary>
        /// Mothod for getting Blue parameter in byte format
        /// from HTML Color in string format
        /// </summary>
        /// <param name="inputColor">HTML Color</param>
        /// <returns>Blue parameter</returns>
        public static byte getBlue(this string inputColor)
        {
            if (inputColor == null) return 0;
            if (!inputColor.IsValidHtmlColor()) return 0;

            Color color = ColorTranslator.FromHtml(inputColor);

            return color.B;
        }
    }
}
