﻿<Window
    x:Class="NaxexColorPicker.View.MainWindow"
    
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
    
    xmlns:sys="clr-namespace:System;assembly=mscorlib"
    
    xmlns:ViewModel="clr-namespace:NaxexColorPicker.ViewModel"
    xmlns:Converters="clr-namespace:NaxexColorPicker.ViewModel.Converters"
    
    Title="Naxex WPF Assignment: Color Picker"
    
    Name="ColorPickerWindow"
    
    Height="240" Width="400"
    MinHeight="185" MinWidth="180"
    MaxHeight="450" MaxWidth="550"
    mc:Ignorable="d">
    
    <Window.Resources>
        <!-- Static Resources -->
        <ViewModel:MainViewModel x:Key="MainViewModel"/>
        <Converters:HtmlColorToBrushConverter x:Key="HtmlColorToBrushConverter"/>
        <Converters:ByteToStringConverter x:Key="ByteToStringConverter"/>
        <Converters:ByteToDoubleConverter x:Key="ByteToDoubleConverter"/>
        <Converters:WidthToVisibilityConverter x:Key="WidthToVisibilityConverter"/>
        
        <!-- Relative Values-->
        <sys:Double x:Key="MinWidth">320</sys:Double>
        <sys:Double x:Key="AbsMinWidth">240</sys:Double>
        <sys:Double x:Key="MinHeight">220</sys:Double>
        
        <!-- Templates -->
        <DataTemplate x:Key="ColorTemplate">
            <Button
                Background="{Binding ColorContent, Converter={StaticResource HtmlColorToBrushConverter} }"
                Command="{Binding DataContext.SelectColorCommand, RelativeSource={RelativeSource AncestorType=Grid, Mode=FindAncestor}}"
                CommandParameter="{Binding ColorContent}"
                
                ToolTip="{Binding ColorTitle}"
                
                Style="{StaticResource ColorButton}"
                
                Margin="5,5,10,10"
                Height="30"
                Width="30"
                d:Content="{Binding ColorTitle}"
                />
        </DataTemplate>

        <ItemsPanelTemplate x:Key="ColorPalettePanel">
            <StackPanel Orientation="Horizontal" Margin="0,0,0,0"/>
        </ItemsPanelTemplate>
        
        <ItemsPanelTemplate x:Key="ColorPaletteMultilinePanel">
            <WrapPanel/>
        </ItemsPanelTemplate>
        
    </Window.Resources>
    
    <Grid
        DataContext="{StaticResource MainViewModel}"
        Margin="3"
        >

        <Grid.RowDefinitions>
            <RowDefinition Height="*"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
        </Grid.RowDefinitions>

        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="Auto" />
            <ColumnDefinition Width="Auto" />
            <ColumnDefinition Width="*"/>
            <ColumnDefinition Width="Auto"/>
        </Grid.ColumnDefinitions>



        <!-- Predefined Colors Panel -->

        <ScrollViewer VerticalScrollBarVisibility="Auto" Grid.ColumnSpan="4" Margin="3"
                      Visibility="{Binding ElementName=ColorPickerWindow, Path=ActualHeight, UpdateSourceTrigger=PropertyChanged, Mode=OneWay,
                      Converter={StaticResource WidthToVisibilityConverter}, ConverterParameter={StaticResource MinHeight}}"
                      >
            <ItemsControl
                ItemsSource="{Binding Colors}"
            
                ItemTemplate="{StaticResource ColorTemplate}"
                ItemsPanel="{StaticResource ColorPaletteMultilinePanel}"
                />
        </ScrollViewer>

        <!-- RGB Values Panel -->
        
        <!-- Red Sub Pandel -->

        <TextBlock
            Text="Red:" Foreground="Red"
            
            Visibility="{Binding ElementName=ColorPickerWindow, Path=ActualWidth, UpdateSourceTrigger=PropertyChanged, Mode=OneWay,
            Converter={StaticResource WidthToVisibilityConverter}, ConverterParameter={StaticResource AbsMinWidth}}"
            
            Grid.Column="0" Grid.Row="1"
            VerticalAlignment="Center" HorizontalAlignment="Right"/>
        
        <TextBox
            Text="{Binding Red, Converter={StaticResource ByteToStringConverter}, UpdateSourceTrigger=PropertyChanged, Mode=TwoWay}"
            Visibility="{Binding ElementName=ColorPickerWindow, Path=ActualWidth, UpdateSourceTrigger=PropertyChanged, Mode=OneWay,
            Converter={StaticResource WidthToVisibilityConverter}, ConverterParameter={StaticResource MinWidth}}"
            
            Width="60"
            Grid.Column="1" Grid.Row="1"
            VerticalAlignment="Center" HorizontalAlignment="Stretch"
            Margin="3"/>
        
        <Slider
            Value="{Binding Red, Converter={StaticResource ByteToDoubleConverter}, UpdateSourceTrigger=PropertyChanged, Mode=TwoWay}"
            ToolTip="{Binding Red, Converter={StaticResource ByteToStringConverter}, UpdateSourceTrigger=PropertyChanged, Mode=OneWay}"

            Minimum="0" Maximum="255"
            Grid.Column="2" Grid.Row="1" Grid.ColumnSpan="2"
            VerticalAlignment="Center"
            Margin="3"/>

        <!-- Gredn Sub Panel -->
        <TextBlock
            Text="Green:" Foreground="Green"
            
            Visibility="{Binding ElementName=ColorPickerWindow, Path=ActualWidth, UpdateSourceTrigger=PropertyChanged, Mode=OneWay,
            Converter={StaticResource WidthToVisibilityConverter}, ConverterParameter={StaticResource AbsMinWidth}}"
            
            Grid.Column="0" Grid.Row="2"
            VerticalAlignment="Center" HorizontalAlignment="Right"/>
        
        <TextBox
            Text="{Binding Green, UpdateSourceTrigger=PropertyChanged, Mode=TwoWay, Converter={StaticResource ByteToStringConverter}}"
            Visibility="{Binding ElementName=ColorPickerWindow, Path=ActualWidth, UpdateSourceTrigger=PropertyChanged, Mode=OneWay,
            Converter={StaticResource WidthToVisibilityConverter}, ConverterParameter={StaticResource MinWidth}}"
            
            Width="60"
            Grid.Column="1" Grid.Row="2"
            VerticalAlignment="Center" HorizontalAlignment="Stretch"
            Margin="3"/>
        
        <Slider
            Value="{Binding Green, UpdateSourceTrigger=PropertyChanged, Mode=TwoWay, Converter={StaticResource ByteToDoubleConverter}}"
            ToolTip="{Binding Green, Converter={StaticResource ByteToStringConverter}, UpdateSourceTrigger=PropertyChanged, Mode=OneWay}"
            
            Minimum="0" Maximum="255"
            Grid.Column="2" Grid.Row="2" Grid.ColumnSpan="2"
            VerticalAlignment="Center"
            Margin="3"/>

        
        <!-- Blue Sub Panel -->
        <TextBlock
            Text="Blue:" Foreground="Blue"
            
            Visibility="{Binding ElementName=ColorPickerWindow, Path=ActualWidth, UpdateSourceTrigger=PropertyChanged, Mode=OneWay,
            Converter={StaticResource WidthToVisibilityConverter}, ConverterParameter={StaticResource AbsMinWidth}}"
            
            Grid.Column="0" Grid.Row="3"
            VerticalAlignment="Center" HorizontalAlignment="Right"/>
        
        <TextBox
            Text="{Binding Blue, UpdateSourceTrigger=PropertyChanged, Mode=TwoWay, Converter={StaticResource ByteToStringConverter}}"
            Visibility="{Binding ElementName=ColorPickerWindow, Path=ActualWidth, UpdateSourceTrigger=PropertyChanged, Mode=OneWay,
            Converter={StaticResource WidthToVisibilityConverter}, ConverterParameter={StaticResource MinWidth}}"
            
            Width="60"
            Grid.Column="1" Grid.Row="3"
            VerticalAlignment="Center" HorizontalAlignment="Stretch"
            Margin="3"/>
        
        <Slider
            Value="{Binding Blue, UpdateSourceTrigger=PropertyChanged, Mode=TwoWay, Converter={StaticResource ByteToDoubleConverter}}"
            ToolTip="{Binding Blue, Converter={StaticResource ByteToStringConverter}, UpdateSourceTrigger=PropertyChanged, Mode=OneWay}"
            
            Minimum="0" Maximum="255"
            Grid.Column="2" Grid.Row="3" Grid.ColumnSpan="2"
            VerticalAlignment="Center"
            Margin="3"/>


        <!-- Result Panel -->

        <TextBlock
            Text="Reslut:"
            Visibility="{Binding ElementName=ColorPickerWindow, Path=ActualWidth, UpdateSourceTrigger=PropertyChanged, Mode=OneWay,
            Converter={StaticResource WidthToVisibilityConverter}, ConverterParameter={StaticResource MinWidth}}"
            
            Grid.Column="0" Grid.Row="4"
            VerticalAlignment="Center" HorizontalAlignment="Right"
            />
        
        <TextBlock
            Text="{Binding SelectedColor}"
            Visibility="{Binding ElementName=ColorPickerWindow, Path=ActualWidth, UpdateSourceTrigger=PropertyChanged, Mode=OneWay,
            Converter={StaticResource WidthToVisibilityConverter}, ConverterParameter={StaticResource MinWidth}}"
            
            
            Grid.Column="1" Grid.Row="4"
            HorizontalAlignment="Center"
            VerticalAlignment="Center"
            Margin="3"/>

        <Button
            Background="{Binding SelectedColor, Converter={StaticResource HtmlColorToBrushConverter}, UpdateSourceTrigger=PropertyChanged, Mode=OneWay}"
            Command="{Binding CopyColorCommand}"
            
            Style="{StaticResource ColorButton}"
            
            ToolTip="Click To Copy HTML Color"
            
            Width="50"
            Height="50"
                
            HorizontalAlignment="Left"
            Grid.Column="2" Grid.Row="4" Grid.RowSpan="2"
            Margin="5"
            />

        <Button
            Content="Close"
            Command="{Binding CloseCommand}"
            
            ToolTip="Close Color Picker"
            
            Visibility="{Binding ElementName=ColorPickerWindow, Path=ActualWidth, UpdateSourceTrigger=PropertyChanged, Mode=OneWay,
                      Converter={StaticResource WidthToVisibilityConverter}, ConverterParameter={StaticResource MinHeight}}"
            
            Grid.Column="3" Grid.Row="5"
            Margin="3"/>
       

    </Grid>
    
</Window>
